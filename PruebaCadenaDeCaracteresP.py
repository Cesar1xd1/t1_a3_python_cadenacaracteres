'''
Created on 25 sep. 2020

@author: Cesar
'''
class CadenaDeCaracteres:
    
    def __init__(self, cadena):
        self.__cadena=cadena
    
    def getCadena(self):
        return self.__cadena
    
    def setCadena(self, cadena):
        self.__cadena=cadena
        
    def cadenaInvertida(self):
        temporal = reversed(self.getCadena())
        print("[", end="")
        for e in temporal:
            for i in range(len(e)-1,-1,-1):
                print(e[i], end="")
            print("    ",end="")
        print("]", end="")
        print()    
        
    def eliminarCaracterPosicion(self, x, z):
        x=x-1
        z=z-1
        cadena=self.getCadena()
        while(x>=len(cadena)):
            print("Estas fuera del rango! ingresa otra posicion: ")
            x=(self.validacion()-1)
        
        fragme=cadena[x]
        while(z>len(fragme)):
            print("Estas fuera del rango! ingresa otra posicion: ")
            z=(self.validacion()-1)
        
        
        if(z==0):
            fragme=fragme[1:]
        elif(z==len(fragme)-1):
            fragme=fragme[:-2]
        else:
            fragme=fragme[0:z]+fragme[z+1:]
        
        cadena[x]=fragme
        self.setCadena(cadena)
    
    def eliminarfragmentoPosiscion(self, y):
        y=y-1
        cadena=self.getCadena()
        while(y>=len(cadena)):
            print("Estas fuera del rango! ingresa otra posicion: ")
            sub=self.validacion()-1
        
        if(sub==0):
            for i in range(len(cadena)-1):
                cadena[i]=cadena[i+1]
            
        elif(sub==len(cadena)-1):
            pass
        else:
            for i in range(len(cadena)-1):
                if(i>=y):
                    cadena[i]=cadena[i+1]
        cadena.pop()
        self.setCadena(cadena)
    
    def mayusYMinus(self):
        s=""
        cadena=self.getCadena()
        for i in range(len(cadena)):
            s=s+cadena[i]+" "
        
        cadMa=s.upper()
        cadMi=s.lower()
        m=0
        
        for i in range(len(s)):
            if (s[i]!=' '):
                m+=1
            if(m%2!=0):
                print(cadMa[i],end="")
            else: 
                print(cadMi[i],end="") 
        print()
    
    def primeraMayuscula(self):
        cadena=self.getCadena()
        for i in range(len(cadena)):
            print(cadena[i].capitalize(),end="    ")
        print()
        
    def validacion(self):
        feil=1
        while feil==1:
            try:
                re = int(input())
            except:
                print("Ese no es un numero entero positivo, intenta de nuevo")
                feil=1
            else:
                if re>0:
                    feil=0
                else:
                    print("Ese no es un numero entero positivo, intenta de nuevo")
                    feil=1
        return re
cdc = CadenaDeCaracteres([""])
opcion = 0

print("Indique el tamaño del vector: ")
tam = cdc.validacion()

print("ingresa las palabras para llenar el vector: ")
cad=[]          
for i in range(tam):
    cad.append(str(input(f"ingresa la palabra {i+1}: ")))
    
cdc1 = CadenaDeCaracteres(cad)

while opcion!=6:
    print("================ MENU ================")
    print("1 para invertir la cadena")
    print("2 Eliminar el caracter de una posicion especifica ")
    print("3 Eliminar el fragmento de una posicion especifica")
    print("4 Convertir mayusculas y minusculas de forma peculiar")
    print("5 Convertir primer letra a mayuscula")
    print("6 ***SALIR*** del programa ")
    
    opcion = cdc.validacion()
    
    if opcion<1 or opcion>6:
        print("Ups! opcion invalida")
    elif opcion==1:
        cdc1.cadenaInvertida()
    elif opcion==2:
        print("ingrese la posicion del fragmento: ")
        x = cdc.validacion()
        print("ingrese la posicion del caracter dentro del fragmento")
        z = cdc.validacion()
        cdc1.eliminarCaracterPosicion(x, z)   
    elif opcion==3:
        print("ingrese la posicion del fragmento: ")
        y = cdc.validacion()
        cdc.eliminarfragmentoPosiscion(y)
    elif opcion==4:
        cdc1.mayusYMinus()()
    elif opcion==5:
        cdc1.primeraMayuscula()
print("El programa ah finalizado")